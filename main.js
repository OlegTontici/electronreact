const { app } = require('electron');
const { BrowserWindow }  = require('electron');

app.on('ready',function()
{
    mainWindow = new BrowserWindow({width:1024,height:768, frame:true})
    mainWindow.on('closed',()=>
    {
        app.exit();
    })
    mainWindow.loadURL('file://' + __dirname + '/index.html');
    mainWindow.webContents.openDevTools();   

    mainWindow.setMenuBarVisibility(false);
    mainWindow.setAutoHideMenuBar(true);
})
